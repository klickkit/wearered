from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=20, null=False)
    phoneNumber = models.IntegerField(null=False)
    age = models.IntegerField(null=False)
    weight = models.IntegerField(null=False)


class HealthDetails(models.Model):
    user_profile = models.OneToOneField(Profile, on_delete=models.CASCADE, default=None)
    isDiabetic = models.BooleanField(default=False)
    hasBloodPressure = models.BooleanField(default=False)
    isCurrentlyInfected = models.BooleanField(default=False)


class Habits(models.Model):
    user_profile = models.OneToOneField(Profile, on_delete=models.CASCADE, default=None)
    aDrunkard = models.BooleanField(default=False)
    aSmoker = models.BooleanField(default=False)


class Score(models.Model):
    user_profile = models.OneToOneField(Profile, on_delete=models.CASCADE, default=None)
    score = models.IntegerField(default=0)

class Location(models.Model):    
    user_profile = models.ForeignKey(Profile, on_delete=models.CASCADE, default=None)
    latitude = models.DecimalField(max_digits=15, decimal_places=10)
    longitude = models.DecimalField(max_digits=15, decimal_places=10)
    