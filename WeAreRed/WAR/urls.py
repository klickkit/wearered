from django.conf.urls import include
from django.conf.urls import url

from WAR.views import get_leaderboard, save_profile

urlpatterns = [
	url(r'^submit', save_profile, name='save_profile'),
	url(r'^leaderboard', get_leaderboard, name='get_leaderboard'),
]