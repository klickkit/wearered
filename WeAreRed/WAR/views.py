from django.shortcuts import render

from django.contrib import auth
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
from django.db.models.signals import post_save, post_delete 

from WAR.models import Habits, HealthDetails, Location, Profile, Score

from redisco.containers import SortedSet


from functools import partial

# XML libraries for converting xml into json and parsing it
from xml.dom.minidom import parseString
import xmlrpclib
import xml.etree.ElementTree as ET


import json
import operator
import redis
import redisco

# Utility functions

leaderboard = SortedSet('leaderboard')
profile_keys = ['name', 'age', 'weight', 'phoneNumber']
health_data_keys = ['isDiabetic', 'hasBloodPressure']
health_data_dict = {}
temp_dict = {}
profile_dict = {}
HABITS_THRESHOLD = 5;
LEADERBOARD_SIZE_TO_BE_DISPLAYED = 10

# Redis connection setup
redisco.connection_setup(host='localhost', port=6379, db=10)

def _find_key(somejson, key):
    def val(node):
        e = node.nextSibling
        while e and e.nodeType != e.ELEMENT_NODE:
            e = e.nextSibling
        return (e.getElementsByTagName('string')[0].firstChild.nodeValue if e 
                else None)
    foo_dom = parseString(xmlrpclib.dumps((json.loads(somejson),)))
    return [val(node) for node in foo_dom.getElementsByTagName('name') 
            if node.firstChild.nodeValue in key]


def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def get_leaderboard(request):
    if request.method == 'GET':
        leaderboard_list = []
        leaderboard_details = {}
        leaderboard_size = leaderboard.zcard()
        if leaderboard_size < LEADERBOARD_SIZE_TO_BE_DISPLAYED:
            leaderboard_data = leaderboard.zrevrange(0, leaderboard_size - 1)
        else:
            leaderboard_data = leaderboard.zrevrange(0, LEADERBOARD_SIZE_TO_BE_DISPLAYED - 1)
        
        for member in leaderboard_data:
            leaderboard_details[member] = leaderboard.zrevrank(member) + 1
            leaderboard_list.append({"email" : member, "score" : leaderboard_details[member]})
        print leaderboard_list
        return HttpResponse(json.dumps(leaderboard_list), content_type="application/json")


@receiver(post_save, sender=Score)
def store_score(sender, instance, **kwargs):
    key = instance.user_profile.user.email
    value = instance.score
    leaderboard.add(key, value)
    print leaderboard.members
    
    
@csrf_exempt
def save_profile(request):
    if request.method == 'POST':
        received_json_data = json.loads(request.body)
        user_data = dict(byteify(received_json_data))
        username = user_data['username']
        email = user_data['email']
        score = user_data['score']
        alcohol_measure = user_data['alcoholMeasure']
        smoke_measure = user_data['smokeMeasure']
        user = auth.authenticate(username=username, password=email)
        if user is not None:
           auth.login(request, user)
        else:
            print "No such user present"
            user = User.objects.create_user(username=username, password=email, email=email)
            auth.login(request, user)
            for key in profile_keys:
                profile_dict[key] = user_data[key]
            print profile_dict
            profile_obj = Profile(**profile_dict)
            profile_obj.user = user
            profile_obj.save()
            # Store score and insert into the Leaderboard
            score_obj = Score(user_profile=profile_obj, score=int(score))
            score_obj.save()
            
            habits_obj = Habits()
            if alcohol_measure >= HABITS_THRESHOLD:
                habits_obj.aDrunkard = True
            if smoke_measure >= HABITS_THRESHOLD:
                habits_obj.aSmoker = True
            habits_obj.user_profile = profile_obj
            habits_obj.save()
            for key in health_data_keys:
                health_data_dict[key] = user_data[key]
            health_details_obj = HealthDetails(**health_data_dict)
            health_details_obj.user_profile = profile_obj
            health_details_obj.save()    
            for data in user_data:
                if data == 'places':
                    for location in user_data[data]:
                        location_obj = Location(**location)
                        location_obj.user_profile = profile_obj
                        location_obj.save()
            return HttpResponse("Profile Saved Successfully")
        
